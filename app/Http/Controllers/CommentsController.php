<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use App\Providers\AuthServiceProvider;

class CommentsController extends Controller
{
    public function update(Comment $comment) {
        try {
            $this->authorize('update', $comment);
        } catch (AuthorizationException $e) {

        }
    }
}
