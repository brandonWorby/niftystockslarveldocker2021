<?php

namespace App\Http\Controllers;

use App\Classes\StockApi;
use App\Interfaces\StockInterface;
use App\Models\Comment;
use Illuminate\Http\Request;
use App\Models\Stock;
use Illuminate\Support\Facades\Storage;
//use Ramsey\Collection\Collection;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

//use App\Models\Comments;
//use Illuminate\Support\Facades\Http;
//use App\Repositories\StockRepository;

class StockController extends Controller
{

    protected StockApi $stockApi;
    protected StockInterface $stockInterface;

    public function __construct(StockInterface $stockInterface, StockApi $stockApi)
    {
        $this->stockApi = $stockApi;
        $this->stockInterface = $stockInterface;
    }

    function stocks() {
        $stocks = Stock::all();
        return view("stocks/stocks" , compact('stocks') );
    }

    function stock(string $symbol) {
        $stock = $this->stockInterface->getStockBySymbol($symbol);
        return view("stocks/stock" , compact('stock') );
    }

    function search( Request $request )
    {
        $query = $request->get('query');
        if( strlen($query)<2) {
            return "";
        }
        $query = strtoupper($query);
        $data = $this->stockApi->getSymbols();
        $collection = new Collection(json_decode($data));

        $results = $collection->filter( function( $item ) use ($query) {
            return strpos($item->symbol, $query) === 0 || strpos($item->name, $query) === 0;
        })->flatten()->toArray();

        return response()->json($results);
    }

    function quoteAsGet( Request $request ) {
        return response()->json($this->stockApi->quote($request->get("symbol") ));
    }

    function getChartData( Request $request ) {
        return response()->json($this->stockApi->chart($request->get("symbol"), $request->get("chartType") ));
    }


    function quoteAsPost() {
        return response()->json(['success'=>true]);
    }

    function addComment( Request $request ) {
        $comment = Comment::create( array_merge( $request->all() , ["user_id"=>Auth::id()] ) );
        return response()->json($comment);
    }

}
