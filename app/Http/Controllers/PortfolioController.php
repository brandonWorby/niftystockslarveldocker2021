<?php

namespace App\Http\Controllers;

use App\Models\Portfolio;
use App\Models\Stock;
use App\Models\StockToPortfolios;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PortfolioController extends Controller
{
    //

    public function portfolios() {
        $id = Auth::id();
        $portfolios = Portfolio::all()->where("user_id",$id);
        return view("portfolios/portfolios" , compact('portfolios') );
    }

    public function portfolio( int $id ) {
        $portfolio = Portfolio::with( ["StockToPortfolios","StockToPortfolios.stock"] )->where([["portfolios.id",$id],["portfolios.user_id", Auth::id()]])->first();
        return view("portfolios/portfolio" , compact('portfolio') );
    }

    public function createPortfolio( Request $request ) {
        // Form validation
        $this->validate($request, [
            'name' => 'required',
        ]);

        //  Store data in database
        Portfolio::create(array_merge($request->all(),["user_id"=>Auth::id()]));

        return back()->with('success', 'Your form has been submitted.');
    }

    public function addStockToPortfolio(Request $request) {
        $portfolio_id = $request->get("portfolio_id");
        $symbol = $request->get("symbol");
        $name = $request->get("name");
        $stock = Stock::firstOrCreate(["symbol"=>$symbol,"name"=>$name]);

        //validate -- make sure portfolio is for logged in user.. if pass -
        StockToPortfolios::create(["user_id"=>Auth::id(),"portfolio_id"=>$portfolio_id,"stock_id"=>$stock->id]);

        return response()->json(["portfolio_id"=>$portfolio_id,"symbol"=>$symbol]);
    }

}
