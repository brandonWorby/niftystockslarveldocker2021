<?php

namespace App\Repositories;

use App\Http\Requests\StockRequest;
use App\Models\Stock;
use App\Classes\StockApi;
use App\Interfaces\StockInterface;

class StockRepository implements StockInterface
{

    protected StockApi $stockApi;

    public function __construct(StockApi $stockApi)
    {
        $this->stockApi = $stockApi;
    }

    /**
     * @inheritDoc
     */
    public function getAllStocks()
    {
        // TODO: Implement getAllStocks() method.
    }

    /**
     * @inheritDoc
     */
    public function getStockBySymbol($symbol) : Stock
    {
        $stock = Stock::with(['comments' => function ($query) {
            $query->whereNull("root_comment_id");
        }])->where('symbol',strtoupper($symbol))->first();

        if(!$stock) {
            $response = $this->stockApi->quote($symbol);
            $stock = new Stock(['symbol' => $response['symbol'], 'name' => $response['companyName']]);
            $stock->save();
        }
        return $stock;
    }

    /**
     * @inheritDoc
     */
    public function requestStock(StockRequest $request, $symbol = null)
    {
        // TODO: Implement requestStock() method.
    }
}
