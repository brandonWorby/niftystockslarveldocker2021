<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Stock;

class StockToPortfolios extends Model
{
    use HasFactory;

    protected $fillable = ['stock_id','portfolio_id','user_id','sequence'];

    public function stock() {
        //return $this->belongsTo(Stock::class);
        return $this->belongsTo(Stock::class, 'stock_id');
    }

    public function portfolios() {
        return $this->belongsTo(Portfolio::class);
    }

}
