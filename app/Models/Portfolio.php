<?php

namespace App\Models;

use App\Models\Stock;
use App\Models\StockToPortfolios;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    use HasFactory;

    protected $fillable = ['name','index','user_id'];

    public function stockToPortfolios() {
        return $this->hasMany(StockToPortfolios::class)->orderBy("sequence");
    }

//    public function stocks() {
//        return $this->belongsToMany(Stock::class, 'stock_to_portfolios', 'portfolio_id', 'stock_id');
//    }

}
