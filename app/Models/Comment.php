<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    protected $fillable = ['comment','reply_to_comment_id','stock_id','root_comment_id','user_id'];


    //public function comments() {
        //return $this->belongsTo(Comments::class , 'root_comment_id');
    //}
    //return $this->belongsToMany('App\Role', 'user_roles', 'user_id', 'foo_id');

    public function replies(){
        return $this->hasMany(Comment::class,'reply_to_comment_id');
    }

}
