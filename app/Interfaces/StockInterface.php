<?php

namespace App\Interfaces;

use App\Http\Requests\StockRequest;


interface StockInterface
{

    /**
     * Get all stocks
     *
     * @method  GET api/stocks
     * @access  public
     */
    public function getAllStocks();

    /**
     * Get stock By Symbol
     *
     * @param   string     $symbol
     *
     * @method  GET api/stocks/{symbol}
     * @access  public
     */
    public function getStockBySymbol($symbol);

    /**
     * Create | Update stock
     *
     * @param   \App\Http\Requests\stockRequest    $request
     * @param   string                           $symbol
     *
     * @method  POST    api/stocks       For Create
     * @method  PUT     api/stocks/{symbol}  For Update
     * @access  public
     */
    public function requestStock(stockRequest $request, $symbol = null);

}
