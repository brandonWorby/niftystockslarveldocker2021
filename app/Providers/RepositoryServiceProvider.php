<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Classes\Api\CloudSSE;
use App\Classes\StockApi;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {

        $this->app->singleton(CloudSSE::class, function ($app) {
            return new CloudSSE(config('app.stock_api_configs.cloud_sse.url'),config('app.stock_api_configs.cloud_sse.token'));
        });

        // Register Interface and Repository in here
        // You must place Interface in first place
        // If you dont, the Repository will not get readed.
        $this->app->bind(
            'App\Interfaces\StockInterface',
            'App\Repositories\StockRepository'
        );

        $this->app->bind(
            'App\Classes\StockApi',
            'App\Classes\Api\CloudSSE'
        );

    }
}
