<?php

namespace App\Classes;

abstract class StockApi
{

    protected string $url;
    protected string $token;

    public function __construct( string $url , string $token )
    {
        $this->url = $url;
        $this->token = $token;
    }

    abstract public function getSymbols();
    abstract public function quote( string $symbol );
    abstract public function chart( string $symbol, string $chartType );
    abstract public function logo( string $symbol );

}
