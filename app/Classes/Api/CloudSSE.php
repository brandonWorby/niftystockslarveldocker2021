<?php

namespace App\Classes\Api;

use App\Classes\StockApi;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class CloudSSE extends StockApi
{

    //https://cloud-sse.iexapis.com/stable/

    public function getSymbols(){
        $symbols_response = null;
        $file_path = "stock_cache_symbols/stock_symbols.json";
        //if( !Storage::exists($file_path) ||  Storage::lastModified($file_path) <= strtotime('-1 minute') ) {
        if( !Storage::exists($file_path) ||  Storage::lastModified($file_path) <= strtotime('-24 hours') ) {
            $url = $this->url.'ref-data/symbols?token='.$this->token;
            $symbols_response = Http::get($url);
            if( $symbols_response->status() != 200 ) {
                throw new \ErrorException("Api-bad response" . $symbols_response->status() );
            }
            $body = $symbols_response->body();
            if(!Storage::put($file_path,$body)){
                chmod($file_path, 0755);
                throw new \ErrorException("Could not get or save file");
            }
        } else {
            $symbols_response = Storage::get($file_path);
        }
        return $symbols_response;
    }


    public function quote(string $symbol)
    {
        $response = Http::get($this->url.'/stock/'.$symbol.'/quote?token='.$this->token);
        return $response->json();
        /*
        $response = Http::get('https://cloud-sse.iexapis.com/stable/stock/aapl/quote?token=pk_953f09e2be254438ac8bef5b2394d829');
        $jsonData = $response->json();
        print_r( $jsonData ); die();
        */
        // TODO: Implement quote() method.
    }

    public function chart( string $symbol, string $chartType ) {
        $response = Http::get($this->url.'/stock/'.$symbol.'/chart/'.$chartType.'?token='.$this->token);
        return $response->json();
    }


    public function logo(string $symbol)
    {
        // TODO: Implement logo() method.
    }


}
