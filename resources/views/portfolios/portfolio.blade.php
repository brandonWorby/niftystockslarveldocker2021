@extends('layouts.app')
@section('content')

    <h1>{{$portfolio->name}}</h1>


    <div class="container">
        <input class="add_to_port form-control" type="text" placeholder="Search">
    </div>

    <ul>
        @foreach($portfolio->stockToPortfolios as $stockToPortfolio)
            <li>
                <a href="/stocks/{{$stockToPortfolio->stock->symbol}}">{{$stockToPortfolio->stock->symbol}}</a>
            </li>
        @endforeach
    </ul>


    <script type="text/javascript">
        var path = "{{ route('stocksdata.search') }}";
        $('input.add_to_port').typeahead({
            source:  function (query, process) {
                return $.get(path, { query: query }, function (data) {
                    //var output = $.map(data, function (string) { return { value: string }; });
                    //console.log(output);
                    //return process(output);
                    return process(data);
                });
            },
            displayText: function(item) {
                var string = item.symbol + " - " + item.name;
                return "+ " + string;
            },
            minLength:2,
            updater: function (item) {
                $('.add_to_port').val("");
                //window.location.href = "/stocks/"+item.symbol;
                console.log("add stock");
                console.log(item);
                //return item.symbol + " - " + item.name;
                var _token = $("input[name='_token']").val();
                $.ajax({
                    url: "{{ route('addStockToPortfolio') }}",
                    type: 'POST',
                    data: {_token: _token,portfolio_id:{{$portfolio->id}} , name: item.name , symbol: item.symbol},
                    success: function (data) {
                        console.log(data);
                    }
                });
            }
        });

        $('typeahead:selected', function (event, data) {
            $('.add_to_port.tt-input').val('');
        }).on('blur', function () {
            $('.add_to_port.tt-input').val('');
        }).on('typeahead:cursorchanged', function (event, data) {
            $('.add_to_port.tt-input').val(data);
        });

    </script>




@endsection


