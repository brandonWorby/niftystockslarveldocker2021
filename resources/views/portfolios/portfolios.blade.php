@extends('layouts.app')
@section('content')

    <ul>
        @foreach($portfolios as $portfolio)
            <li>
                <a href="/portfolio/{{$portfolio->id}}">{{$portfolio->name}}</a>
            </li>
        @endforeach
    </ul>

    @include('portfolios.portfolio_create')

@endsection


