@extends('layouts.app')

<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>

@section('content')


    <div id="testGet">
        testGet
    </div>

    <div id="testPost">
        testPost
    </div>


    <div id="testGetChartData">
        testGetChartData
    </div>

    @include('charts.chart',["stock"=>$stock])

    <!-- Success message -->
    @if(Session::has('success'))
        <div class="alert alert-success">
            {{Session::get('success')}}
        </div>
    @endif

    <h1>{{$stock->symbol}} Page</h1>

    <div class="row">
        <div class="detail-stock-breakdown">

            <div class="row">
                <div class="columns large-2">
                    <h3 class=""><span class="symbol">sym</span></h3>
                </div>
                <div class="columns large-4">
                    <h3 class=""><span class="company-name">name</span></h3>
                </div>
                <div class="columns large-6">
                </div>
            </div>

            <div class="row">
                <div class="columns large-4 small-4">
                    <span class="label current-last-close-label">LC</span>
                    <h1 class="latest-price">$0.0</h1>
                    <h3 class="price-percent-change">+0.00%</h3>
                </div>
                <div class="columns large-4 small-4 extended-hours">
                    <span class="label pm-label">Extended Hours</span>
                    <h1 class="pm-price">$0.0</h1>
                    <h3 class="pm-percent-change">0.0</h3>
                </div>
                <div class="columns large-4 small-4 day-info">
                    <div class="">
                        <div class="label">Date</div>
                        <div class="latest-time">
                            <span class="value"></span>
                        </div>
                    </div>
                    <div class="">
                        <div class="label">High</div>
                        <div class="day-high"><span class="value"></span></div>
                    </div>
                    <div class="">
                        <div class="label">Low</div>
                        <div class="day-low"><span class="value"></span></div>
                    </div>

                </div>
            </div>

        </div>
    </div>


    @include('comments.comments',['comments'=>$stock->comments,"stock_id"=>$stock->id])



    <script type="text/javascript">

        $(document).ready(function() {

            //add_comment_form
            //$(".add-comment-submit").click(function (e) {
            $('.add_comment_form').on('submit', function(e){
                e.preventDefault();
                var root_comment_id = $(this).find('[name=root_comment_id]').val();
                var reply_to_comment_id = $(this).find('[name=reply_to_comment_id]').val();
                var comment = $(this).find('[name=comment]').val();
                var _token = $("input[name='_token']").val();
                console.log(reply_to_comment_id);
                $.ajax({
                    url: "{{ route('stockAddComment') }}",
                    type: 'POST',
                    data: {_token: _token, reply_to_comment_id: reply_to_comment_id, root_comment_id: root_comment_id, stock_id: {{$stock->id}}, comment:comment},
                    success: function (data) {
                        console.log(data);
                        $('.add_comment_form').hide('fast');
                        alert("How to add dynamically and keep user where they are? - reloading a section of view prob not great.");
                    }
                });
            });

            $('.add_comment_form').hide();

            $('.replyButton').click(function (e) {
                console.log('asdf');
                //$(this).next( ".add_comment_form" ).show();
                $(this).next( ".comment-form-container" ).find('.add_comment_form').toggle();
            });

            $("#testGetChartData").click(function (e) {
                var _token = $("input[name='_token']").val();
                $.ajax({
                    url: "{{ route('stocksdata.getChartData') }}",
                    type: 'GET',
                    data: {_token: _token, symbol: "{{$stock->symbol}}", chartType: "1y"},
                    success: function (data) {
                        console.log(data);
                    }
                });

            });

            $("#testGet").click(function (e) {
                var _token = $("input[name='_token']").val();
                $.ajax({
                    url: "{{ route('stocksdata.quoteAsGet') }}",
                    type: 'GET',
                    data: {_token: _token, symbol: "{{$stock->symbol}}"},
                    success: function (data) {
                        console.log(data);

                        $('.latest-price').text(data.latestPrice);
                        //companyName
                        //avgTotalVolume
                        //close
                        //extendedPrice
                        //extendedChangePercent
                        //high
                        //low
                        //latestVolume
                        //avgTotalVolume
                        //changePercent
                        //peRatio
                        //week52High
                        //week52Low
                        //ytdChange
                        //volume
                        //latestPrice
                        //latestSource - close
                        //latestTime

                    }
                });
            });

            $("#testPost").click(function (e) {
                var _token = $("input[name='_token']").val();
                //var email = $("#email").val();
                //var pswd = $("#pwd").val();
                //var address = $("#address").val();

                $.ajax({
                    url: "{{ route('stocksdata.quoteAsPost') }}",
                    type: 'POST',
                    data: {_token: _token, email: "email", pswd: "pswd", address: "address"},
                    success: function (data) {
                        console.log(data);
                    }
                });
            });

        });
    </script>

@endsection
