

<form class="add_comment_form" method="post" action="{{route('editComment')}}" >
    @csrf
    <div class="form-group">
        <label>Edit Comment</label>
        <input type="text" class="form-control" name="comment" id="edit_comment" value="{{$comment->comment}}">
        <input type="hidden" name="comment_id" id="comment_id" value="{{$comment->id}}">
    </div>
    <input type="submit" name="send" value="Save" class="btn btn-dark btn-block add-comment-submit">
</form>

