
    <p>reply_to_comment_id {{$comment->reply_to_comment_id}}</p>
    <p>root_comment_id {{$comment->root_comment_id}}</p>
    <p>{{$comment->comment}}</p>

    <hr>

    @if (auth()->id() === $comment->user_id)
        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#sub_comments_for_{{$comment->id}}" aria-expanded="false" aria-controls="collapseExample">
            edit
        </button>
    @endif

    @if (count($comment->replies))
        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#sub_comments_for_{{$comment->id}}" aria-expanded="false" aria-controls="collapseExample">
            view replies
        </button>
    @endif

    <button class="replyButton btn btn-primary">Reply</button>

    <div class="comment-form-container container-for-comment-id-{{$comment->id}} container mt-5">
        @include('comments.comment_form',["reply_to_comment_id"=>$comment->id,"root_comment_id"=>$comment->id,"stock_id"=>$stock->id])
    </div>

    @if (count($comment->replies))
        <ul class="sub_comments collapse" id="sub_comments_for_{{$comment->id}}">
            <li>
                @include('comments.comments', ['comments' => $comment->replies])
            </li>
        </ul>
    @endif


