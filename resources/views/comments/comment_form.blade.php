

    <form class="add_comment_form" method="post" action="{{route('stockAddComment')}}" >
        @csrf
        <div class="form-group">
            <label>Add Comment</label>
            <input type="text" class="form-control" name="comment" id="comment">
            <input type="hidden" name="reply_to_comment_id" id="reply_to_comment_id" value="{{$reply_to_comment_id}}">
            <input type="hidden" name="root_comment_id" id="root_comment_id" value="{{$root_comment_id}}">
            <input type="hidden" name="stock_id" id="" value="{{$stock_id}}">
        </div>

        <input type="submit" name="send" value="Submit" class="btn btn-dark btn-block add-comment-submit">

    </form>

