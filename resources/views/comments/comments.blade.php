

<div id="comments">

    <ul>
        @foreach($comments as $comment)
            <li class="comment-list-item">
                @include('comments.single_comment',['comment'=>$comment])
            </li>
        @endforeach
    </ul>

</div>
