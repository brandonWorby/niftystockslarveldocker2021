<h1>Chart</h1>





    <div class="row">
        <div class="col-lg-8 col-md-12">
            <div class="chart-container" id="main-chart-container">
                <div class="chart" id="main-chart">
                    <canvas id="{{$stock->symbol}}_chart"></canvas>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-0">
        </div>
    </div>

    <div class="row detail-stock-breakdown">

        <!--<div class="stocks view large-9 medium-8 columns content">-->
        <div class="stock-details view large-8 medium-8 small-12 columns">

            <div class="row chartTypes">

                <div class="columns col-2 chart-change one-day active">
                    1D
                </div>
                <div class="columns col-2 chart-change one-month">
                    1M
                </div>
                <div class="columns col-2 chart-change six-month">
                    6M
                </div>
                <div class="columns col-2 chart-change ytd">
                    YTD
                </div>
                <div class="columns col-2 chart-change one-year">
                    1Y
                </div>
                <!--<div class="columns small-2 chart-change two-year">
                    2Y
                </div>-->
                <div class="columns small-2 chart-change five-year">
                    5Y
                </div>

            </div>

        </div>

        <div class="stock-detils view large-4 medium-4 small-12 columns">

            <div class="chart-info">

            </div>

        </div>

    </div>



<script type="text/javascript">

    $(document).ready(function() {

        var chart;
        var chartIDSelector = "";


        $("#testGetChartData").click(function (e) {

            var _token = $("input[name='_token']").val();
            $.ajax({
                url: "{{ route('stocksdata.getChartData') }}",
                type: 'GET',
                data: {_token: _token, symbol: "{{$stock->symbol}}", chartType: "1y"},
                success: function (data) {
                    //console.log(data);
                    var averages = [];
                    var labels = [];
                    data.forEach(function(item,index) {

                        //need to handle 1d -

                        //all others -
                        if( item.close ) {
                            averages.push( item.close );
                            console.log(" item.close ");
                        } else if( item.uClose ) {
                            averages.push( item.uClose );
                            console.log(" item.uClose ");
                        }
                        labels.push( item.date );
                    });

                    if( chart ) {
                        setupChart(averages, labels, true);
                    } else {
                        setupChart(averages, labels, false);
                    }
                }
            });
        });

        function setupChart(  averages , labels , withDestroy ) {

            if( withDestroy ) {
                chart.destroy();
            }

            var color = 'rgba(255, 187, 0, 1.0)';
            var ctx = document.getElementById("{{$stock->symbol}}_chart").getContext('2d');

            chart = new Chart(ctx, {
                // The type of chart we want to create
                type: 'line',

                data: {
                    labels: labels,
                    datasets: [{
                        radius:2,
                        label: '',
                        backgroundColor: color,
                        borderColor: 'rgb(255, 255, 255)',
                        data: averages
                    }]
                },

                // Configuration options go here
                options: {
                    legend: {
                        display: false,
                        labels: {
                            fontColor: 'rgb(255, 99, 132)'
                        }
                    },
                    title: {
                        display: false,
                        text: 'Custom Chart Title'
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: false,
                            }
                        }]
                    },
                    elements: {
                        line: {
                            tension: 0 // disables bezier curves
                        }
                    }
                }

            });
        }


    });

</script>
