<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateInitialTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //database/my_sql_files/inital_sql.sql
        DB::unprepared(file_get_contents( dirname(__FILE__) . '/../my_sql_files/inital_sql.sql'));

        //ini_set('memory_limit', '-1');
        //\DB::unprepared( file_get_contents( "link/to/dump.sql" ) );
    }

}
