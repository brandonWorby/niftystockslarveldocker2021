

# Replace placeholder table for user_ratings_summary with correct view syntax
# ------------------------------------------------------------

DROP TABLE `user_ratings_summary`;

CREATE ALGORITHM=UNDEFINED VIEW `user_ratings_summary`
AS SELECT
       `users`.`id` AS `id`,avg(`ratings`.`value`) AS `rating_average`
   FROM (`users` join `ratings` on((`ratings`.`for_user_id` = `users`.`id`))) group by `users`.`id`;


# Replace placeholder table for user_reputation_summary with correct view syntax
# ------------------------------------------------------------

DROP TABLE `user_reputation_summary`;

CREATE ALGORITHM=UNDEFINED VIEW `user_reputation_summary`
AS SELECT
       `users`.`id` AS `user_id`,avg(`ratings`.`value`) AS `ratings_average`,(select count(0)
                                                                              FROM `comment_flags` where ((`comment_flags`.`flagged_user_id` = `users`.`id`) and `comment_flags`.`flag_type_id` in (select `flag_types`.`id` from `flag_types` where (`flag_types`.`positive` = 0)))) AS `negitive_flags_count`,(select count(0) from `comment_flags` where ((`comment_flags`.`flagged_user_id` = `users`.`id`) and `comment_flags`.`flag_type_id` in (select `flag_types`.`id` from `flag_types` where (`flag_types`.`positive` = 1)))) AS `positive_flags_count`,(select count(0) from `votes` where ((`votes`.`comment_owner_user_id` = `votes`.`user_id`) and (`votes`.`value` = 1))) AS `up_votes_count`,(select count(0) from `votes` where ((`votes`.`comment_owner_user_id` = `votes`.`user_id`) and (`votes`.`value` = 0))) AS `down_votes_count` from (`users` join `ratings` on((`ratings`.`for_user_id` = `users`.`id`))) group by `users`.`id`;
