# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.31)
# Database: nifty_stocks_api
# Generation Time: 2020-12-14 01:55:17 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table comment_flags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `comment_flags`;

CREATE TABLE `comment_flags` (
                                 `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                                 `flagged_user_id` int(11) unsigned NOT NULL,
                                 `flagged_by_user_id` int(11) unsigned DEFAULT NULL,
                                 `flagged_comment_id` int(11) unsigned DEFAULT NULL,
                                 `flag_type_id` int(11) unsigned NOT NULL,
                                 `created` timestamp NULL DEFAULT NULL,
                                 `modified` datetime DEFAULT CURRENT_TIMESTAMP,
                                 `deleted` tinyint(1) DEFAULT '0',
                                 PRIMARY KEY (`id`),
                                 KEY `comment_flags_flagged_user_id_fk_1` (`flagged_user_id`),
                                 KEY `comment_flags_flagged_by_user_id_fk_2` (`flagged_by_user_id`),
                                 KEY `comment_flags_flagged_comment_id_fk_3` (`flagged_comment_id`),
                                 KEY `comment_flags_flag_type_id_fk_4` (`flag_type_id`),
                                 CONSTRAINT `comment_flags_flag_type_id_fk_4` FOREIGN KEY (`flag_type_id`) REFERENCES `flag_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                                 CONSTRAINT `comment_flags_flagged_by_user_id_fk_2` FOREIGN KEY (`flagged_by_user_id`) REFERENCES `users_bu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                                 CONSTRAINT `comment_flags_flagged_comment_id_fk_3` FOREIGN KEY (`flagged_comment_id`) REFERENCES `comments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                                 CONSTRAINT `comment_flags_flagged_user_id_fk_1` FOREIGN KEY (`flagged_user_id`) REFERENCES `users_bu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `comment_flags` WRITE;
/*!40000 ALTER TABLE `comment_flags` DISABLE KEYS */;

INSERT INTO `comment_flags` (`id`, `flagged_user_id`, `flagged_by_user_id`, `flagged_comment_id`, `flag_type_id`, `created`, `modified`, `deleted`)
VALUES
(54,4,4,127,100,'2019-04-06 22:56:26','2019-04-07 03:56:26',0),
(55,4,4,126,101,'2019-04-06 22:57:56','2019-04-07 03:57:56',0),
(56,3,4,127,100,'2019-04-06 22:56:26','2019-04-07 03:56:26',0),
(57,3,4,127,103,'2019-04-06 22:56:26','2019-04-07 03:56:26',0);

/*!40000 ALTER TABLE `comment_flags` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table comments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `comments`;

CREATE TABLE `comments` (
                            `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                            `comment` text,
                            `user_id` bigint(20) unsigned NOT NULL,
                            `reply_to_comment_id` int(11) unsigned DEFAULT NULL,
                            `root_comment_id` int(11) unsigned DEFAULT NULL,
                            `stock_id` int(11) unsigned NOT NULL,
                            `created` timestamp NULL DEFAULT NULL,
                            `modified` datetime DEFAULT CURRENT_TIMESTAMP,
                            `deleted` tinyint(1) DEFAULT '0',
                            `reported_count` int(10) NOT NULL DEFAULT '0',
                            `created_at` timestamp NULL DEFAULT NULL,
                            `updated_at` timestamp NULL DEFAULT NULL,
                            PRIMARY KEY (`id`),
                            KEY `comments_to_stock_fk_2` (`stock_id`),
                            KEY `comments_reply_to_comment_fk_3` (`reply_to_comment_id`),
                            KEY `comments_root_comment_id_fk_4` (`root_comment_id`),
                            KEY `user_id` (`user_id`),
                            CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
                            CONSTRAINT `comments_reply_to_comment_fk_3` FOREIGN KEY (`reply_to_comment_id`) REFERENCES `comments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                            CONSTRAINT `comments_root_comment_id_fk_4` FOREIGN KEY (`root_comment_id`) REFERENCES `comments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                            CONSTRAINT `comments_to_stock_fk_2` FOREIGN KEY (`stock_id`) REFERENCES `stocks` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;

INSERT INTO `comments` (`id`, `comment`, `user_id`, `reply_to_comment_id`, `root_comment_id`, `stock_id`, `created`, `modified`, `deleted`, `reported_count`, `created_at`, `updated_at`)
VALUES
(57,'asdfasd',4,NULL,NULL,3,NULL,'2019-03-30 23:16:29',0,0,NULL,NULL),
(69,'Comment 69\n',4,70,70,4,'2019-03-31 22:35:28','2019-04-01 03:35:28',0,0,NULL,NULL),
(147,'333333333',1,70,70,4,NULL,'2020-11-29 11:41:34',0,0,'2020-11-29 17:41:34','2020-11-29 17:41:34');

/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table failed_jobs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `failed_jobs`;

CREATE TABLE `failed_jobs` (
                               `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                               `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                               `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
                               `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
                               `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
                               `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
                               `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                               PRIMARY KEY (`id`),
                               UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table flag_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `flag_types`;

CREATE TABLE `flag_types` (
                              `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                              `display_name` varchar(64) NOT NULL DEFAULT '',
                              `name` varchar(64) DEFAULT NULL,
                              `positive` tinyint(1) DEFAULT '0',
                              `created` timestamp NULL DEFAULT NULL,
                              `modified` datetime DEFAULT CURRENT_TIMESTAMP,
                              `deleted` tinyint(1) DEFAULT '0',
                              PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `flag_types` WRITE;
/*!40000 ALTER TABLE `flag_types` DISABLE KEYS */;

INSERT INTO `flag_types` (`id`, `display_name`, `name`, `positive`, `created`, `modified`, `deleted`)
VALUES
(100,'test','test',0,NULL,'2019-04-06 22:45:06',0),
(101,'nasty','nasty',0,NULL,'2019-04-06 22:45:06',0),
(102,'what?','what',0,NULL,'2019-04-06 22:45:06',0),
(103,'NIFTY','what',1,NULL,'2019-04-06 22:45:06',0);

/*!40000 ALTER TABLE `flag_types` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table logs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `logs`;

CREATE TABLE `logs` (
                        `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                        `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                        `name` varchar(32) NOT NULL,
                        `user_id` int(10) unsigned DEFAULT NULL,
                        `request_uri` varchar(255) DEFAULT NULL,
                        `version` varchar(32) DEFAULT NULL,
                        `client_ip` varchar(32) DEFAULT NULL,
                        `platform` varchar(255) DEFAULT NULL,
                        `user_agent` varchar(255) DEFAULT NULL,
                        `message` varchar(255) DEFAULT NULL,
                        `route_data` text,
                        `request_method` varchar(8) DEFAULT NULL,
                        PRIMARY KEY (`id`),
                        UNIQUE KEY `id` (`id`),
                        KEY `user_id_idxfk_7` (`user_id`),
                        CONSTRAINT `event_logs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users_bu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `logs` WRITE;
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;

INSERT INTO `logs` (`id`, `date`, `name`, `user_id`, `request_uri`, `version`, `client_ip`, `platform`, `user_agent`, `message`, `route_data`, `request_method`)
VALUES
(5428,'2019-04-14 00:13:56','basic-info',NULL,'/api/users/token',NULL,'172.20.0.1',NULL,'PostmanRuntime/7.6.1','','{\"email\":\"**\",\"password\":\"**\"}','POST'),
(5750,'2019-05-30 23:41:35','basic-info',3,'/api/portfolios/portfolios',NULL,'172.20.0.1',NULL,'StocksChatThingReactApp/1 CFNetwork/978.0.7 Darwin/18.5.0','','[]','GET');

/*!40000 ALTER TABLE `logs` ENABLE KEYS */;
UNLOCK TABLES;



# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
                                   `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                   `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                   `created_at` timestamp NULL DEFAULT NULL,
                                   KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table photos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `photos`;

CREATE TABLE `photos` (
                          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                          `user_id` int(11) unsigned NOT NULL,
                          `name` varchar(64) DEFAULT '',
                          `extension` varchar(10) DEFAULT NULL,
                          `type` varchar(10) DEFAULT NULL,
                          `file_path` varchar(255) DEFAULT NULL,
                          `file_thumb_exists` tinyint(1) DEFAULT NULL,
                          `file_thumb_path` varchar(255) DEFAULT NULL,
                          `created` timestamp NULL DEFAULT NULL,
                          `modified` datetime DEFAULT CURRENT_TIMESTAMP,
                          `deleted` tinyint(1) DEFAULT '0',
                          `size` int(20) DEFAULT NULL,
                          PRIMARY KEY (`id`),
                          KEY `photos_user_fk_1` (`user_id`),
                          CONSTRAINT `photos_user_fk_1` FOREIGN KEY (`user_id`) REFERENCES `users_bu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `photos` WRITE;
/*!40000 ALTER TABLE `photos` DISABLE KEYS */;

INSERT INTO `photos` (`id`, `user_id`, `name`, `extension`, `type`, `file_path`, `file_thumb_exists`, `file_thumb_path`, `created`, `modified`, `deleted`, `size`)
VALUES
(112,4,'MonsterV3-10-512.png','png','image/png','/var/www/html/webroot/img/public/avatars/08/80/79/20190404-011547-379355-001.png',1,'/var/www/html/webroot/img/public/avatars/08/80/79/20190404-011547-379355-001_thumb.png','2019-04-03 20:15:47','2019-04-04 01:15:47',0,NULL),
(113,4,'RobotV2-71-512.png','png','image/png','/var/www/html/webroot/img/public/avatars/08/80/79/20190404-011558-230443-001.png',1,'/var/www/html/webroot/img/public/avatars/08/80/79/20190404-011558-230443-001_thumb.png','2019-04-03 20:15:58','2019-04-04 01:15:58',0,NULL),
(114,4,'00-07-2-512.png','png','image/png','/var/www/html/webroot/img/public/avatars/08/80/79/20190404-011607-707061-001.png',1,'/var/www/html/webroot/img/public/avatars/08/80/79/20190404-011607-707061-001_thumb.png','2019-04-03 20:16:07','2019-04-04 01:16:07',0,NULL),
(115,4,'Capt_Spaulding-01.png','png','image/png','/var/www/html/webroot/img/public/avatars/08/80/79/20190404-011616-717413-001.png',1,'/var/www/html/webroot/img/public/avatars/08/80/79/20190404-011616-717413-001_thumb.png','2019-04-03 20:16:16','2019-04-04 01:16:16',0,NULL),
(116,4,'Capt_Spaulding-01.png','png','image/png','/var/www/html/webroot/img/public/avatars/08/80/79/20190405-121906-019411-001.png',1,'/var/www/html/webroot/img/public/avatars/08/80/79/20190405-121906-019411-001_thumb.png','2019-04-04 19:19:06','2019-04-05 00:19:06',0,NULL);

/*!40000 ALTER TABLE `photos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table portfolios
# ------------------------------------------------------------

DROP TABLE IF EXISTS `portfolios`;

CREATE TABLE `portfolios` (
                              `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                              `user_id` bigint(20) unsigned NOT NULL,
                              `name` varchar(64) DEFAULT '',
                              `index` int(11) DEFAULT '0',
                              `created` timestamp NULL DEFAULT NULL,
                              `modified` datetime DEFAULT CURRENT_TIMESTAMP,
                              `deleted` tinyint(1) DEFAULT '0',
                              `updated_at` timestamp NULL DEFAULT NULL,
                              `created_at` timestamp NULL DEFAULT NULL,
                              PRIMARY KEY (`id`),
                              KEY `user_id` (`user_id`),
                              CONSTRAINT `portfolios_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `portfolios` WRITE;
/*!40000 ALTER TABLE `portfolios` DISABLE KEYS */;

INSERT INTO `portfolios` (`id`, `user_id`, `name`, `index`, `created`, `modified`, `deleted`, `updated_at`, `created_at`)
VALUES
(88,1,'test Port',0,'2019-03-29 17:17:04','2019-03-29 22:17:04',0,NULL,NULL),
(89,1,'poop',0,'2019-03-29 19:20:56','2019-03-30 00:20:56',0,NULL,NULL),
(93,1,'ssdw',0,'2019-03-29 19:27:33','2019-03-30 00:27:33',0,NULL,NULL),
(95,1,'d3d',0,'2019-03-29 19:29:14','2019-03-30 00:29:14',0,NULL,NULL),
(97,1,'ds',0,'2019-03-29 19:33:52','2019-03-30 00:33:52',0,NULL,NULL),
(104,1,'Poopy test',0,'2019-04-04 20:25:30','2019-04-05 01:25:30',0,NULL,NULL),
(105,1,'test',0,'2019-04-04 21:43:49','2019-04-05 02:43:49',0,NULL,NULL),
(106,1,'hi',0,'2019-05-25 16:33:35','2019-05-25 21:33:35',0,NULL,NULL),
(108,1,'hi222222',0,'2019-05-25 16:36:33','2019-05-25 21:42:49',0,NULL,NULL),
(109,1,'poop',0,'2020-04-05 13:53:19','2020-04-05 18:53:19',0,NULL,NULL),
(111,1,'asdfasd',0,NULL,'2020-11-28 20:34:37',0,'2020-11-29 02:34:37','2020-11-29 02:34:37'),
(112,1,'asdfasd222',0,NULL,'2020-11-28 20:35:13',0,'2020-11-29 02:35:13','2020-11-29 02:35:13'),
(113,1,'test',0,NULL,'2020-11-28 23:13:18',0,'2020-11-29 05:13:18','2020-11-29 05:13:18');

/*!40000 ALTER TABLE `portfolios` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ratings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ratings`;

CREATE TABLE `ratings` (
                           `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                           `value` int(10) NOT NULL,
                           `for_user_id` int(11) unsigned NOT NULL,
                           `from_user_id` int(11) unsigned NOT NULL,
                           `stock_id` int(11) unsigned DEFAULT NULL,
                           `comment_id` int(11) unsigned DEFAULT NULL,
                           `created` timestamp NULL DEFAULT NULL,
                           `modified` datetime DEFAULT CURRENT_TIMESTAMP,
                           `deleted` tinyint(1) DEFAULT '0',
                           PRIMARY KEY (`id`),
                           KEY `ratings_from_user_id_fk_1` (`from_user_id`),
                           KEY `ratings_stock_fk_2` (`stock_id`),
                           KEY `ratings_comment_fk_3` (`comment_id`),
                           KEY `ratings_for_user_id_fk_4` (`for_user_id`),
                           CONSTRAINT `ratings_comment_fk_3` FOREIGN KEY (`comment_id`) REFERENCES `comments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                           CONSTRAINT `ratings_for_user_id_fk_4` FOREIGN KEY (`for_user_id`) REFERENCES `users_bu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                           CONSTRAINT `ratings_from_user_id_fk_1` FOREIGN KEY (`from_user_id`) REFERENCES `users_bu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                           CONSTRAINT `ratings_stock_fk_2` FOREIGN KEY (`stock_id`) REFERENCES `stocks` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `ratings` WRITE;
/*!40000 ALTER TABLE `ratings` DISABLE KEYS */;

INSERT INTO `ratings` (`id`, `value`, `for_user_id`, `from_user_id`, `stock_id`, `comment_id`, `created`, `modified`, `deleted`)
VALUES
(54,2,3,4,NULL,NULL,NULL,'2019-04-08 04:05:19',0);

/*!40000 ALTER TABLE `ratings` ENABLE KEYS */;
UNLOCK TABLES;




# Dump of table status
# ------------------------------------------------------------

DROP TABLE IF EXISTS `status`;

CREATE TABLE `status` (
                          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                          `name` varchar(64) DEFAULT NULL,
                          `display_name` varchar(64) DEFAULT NULL,
                          `code` int(15) NOT NULL,
                          `created` timestamp NULL DEFAULT NULL,
                          `modified` datetime DEFAULT CURRENT_TIMESTAMP,
                          `deleted` tinyint(1) DEFAULT '0',
                          PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `status` WRITE;
/*!40000 ALTER TABLE `status` DISABLE KEYS */;

INSERT INTO `status` (`id`, `name`, `display_name`, `code`, `created`, `modified`, `deleted`)
VALUES
(1,'sdf','SDFSF',0,NULL,'2019-03-28 19:22:50',0);

/*!40000 ALTER TABLE `status` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table stock_to_portfolios
# ------------------------------------------------------------

DROP TABLE IF EXISTS `stock_to_portfolios`;

CREATE TABLE `stock_to_portfolios` (
                                       `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                                       `user_id` bigint(20) unsigned NOT NULL,
                                       `portfolio_id` int(11) unsigned NOT NULL,
                                       `stock_id` int(11) unsigned NOT NULL,
                                       `sequence` int(11) DEFAULT '0',
                                       `created` timestamp NULL DEFAULT NULL,
                                       `modified` datetime DEFAULT CURRENT_TIMESTAMP,
                                       `deleted` tinyint(1) DEFAULT '0',
                                       `updated_at` timestamp NULL DEFAULT NULL,
                                       `created_at` timestamp NULL DEFAULT NULL,
                                       PRIMARY KEY (`id`),
                                       KEY `stock_to_portfolios_list_fk_2` (`portfolio_id`),
                                       KEY `stock_to_portfolios_stock_fk_3` (`stock_id`),
                                       KEY `user_id` (`user_id`),
                                       CONSTRAINT `stock_to_portfolios_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                                       CONSTRAINT `stock_to_portfolios_list_fk_2` FOREIGN KEY (`portfolio_id`) REFERENCES `portfolios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                                       CONSTRAINT `stock_to_portfolios_stock_fk_3` FOREIGN KEY (`stock_id`) REFERENCES `stocks` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `stock_to_portfolios` WRITE;
/*!40000 ALTER TABLE `stock_to_portfolios` DISABLE KEYS */;

INSERT INTO `stock_to_portfolios` (`id`, `user_id`, `portfolio_id`, `stock_id`, `sequence`, `created`, `modified`, `deleted`, `updated_at`, `created_at`)
VALUES
(159,1,97,3,3,'2019-03-29 21:19:19','2019-03-30 02:19:19',0,NULL,NULL),
(161,1,97,4,1,'2019-03-29 21:31:03','2019-03-30 02:31:03',0,NULL,NULL),
(162,1,97,5,2,'2019-03-29 22:45:30','2019-03-30 03:45:30',0,NULL,NULL),
(163,1,89,6,4,'2019-03-30 00:44:53','2019-03-30 05:44:53',0,NULL,NULL),
(164,1,89,7,5,'2019-03-30 00:45:10','2019-03-30 05:45:10',0,NULL,NULL),
(165,1,97,8,0,'2019-04-01 13:28:10','2019-04-01 18:28:10',0,NULL,NULL),
(166,1,97,9,0,'2019-04-01 13:28:42','2019-04-01 18:28:42',0,NULL,NULL),
(167,1,89,5,1,'2019-04-04 22:03:30','2019-04-05 03:03:30',0,NULL,NULL),
(168,1,89,8,2,'2019-04-04 23:10:05','2019-04-05 04:10:05',0,NULL,NULL),
(169,1,89,12,8,'2019-04-04 23:16:52','2019-04-05 04:16:52',0,NULL,NULL),
(170,1,89,13,0,'2019-04-04 23:17:09','2019-04-05 04:17:09',0,NULL,NULL),
(172,1,95,4,0,'2019-04-05 22:59:26','2019-04-06 03:59:26',0,NULL,NULL),
(173,1,95,5,0,'2019-04-05 23:12:26','2019-04-06 04:12:26',0,NULL,NULL),
(174,1,89,23,6,'2019-04-10 20:45:59','2019-04-11 01:45:59',0,NULL,NULL),
(175,1,89,20,3,'2019-04-12 23:40:58','2019-04-13 04:40:58',0,NULL,NULL),
(176,1,89,4,7,'2019-04-15 22:25:25','2019-04-16 03:25:25',0,NULL,NULL),
(177,1,93,4,0,'2019-04-15 22:25:25','2019-04-16 03:25:25',0,NULL,NULL),
(178,1,108,4,3,'2019-04-15 22:25:25','2019-04-16 03:25:25',0,NULL,NULL),
(179,1,108,20,2,'2019-04-15 22:25:25','2019-04-16 03:25:25',0,NULL,NULL),
(180,1,108,13,1,'2019-04-15 22:25:25','2019-04-16 03:25:25',0,NULL,NULL),
(181,1,93,8,2,'2020-04-05 13:49:29','2020-04-05 18:49:29',0,NULL,NULL),
(182,1,93,19,1,'2020-04-05 13:49:47','2020-04-05 18:49:47',0,NULL,NULL),
(184,1,88,30,0,NULL,'2020-11-28 23:04:12',0,'2020-11-29 05:04:12','2020-11-29 05:04:12'),
(185,1,88,4,0,NULL,'2020-11-28 23:10:27',0,'2020-11-29 05:10:27','2020-11-29 05:10:27');

/*!40000 ALTER TABLE `stock_to_portfolios` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table stocks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `stocks`;

CREATE TABLE `stocks` (
                          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                          `symbol` varchar(64) NOT NULL DEFAULT '',
                          `name` varchar(64) DEFAULT NULL,
                          `created` timestamp NULL DEFAULT NULL,
                          `modified` datetime DEFAULT CURRENT_TIMESTAMP,
                          `deleted` tinyint(1) DEFAULT '0',
                          `updated_at` timestamp NULL DEFAULT NULL,
                          `created_at` timestamp NULL DEFAULT NULL,
                          PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `stocks` WRITE;
/*!40000 ALTER TABLE `stocks` DISABLE KEYS */;

INSERT INTO `stocks` (`id`, `symbol`, `name`, `created`, `modified`, `deleted`, `updated_at`, `created_at`)
VALUES
(3,'LYFT','','2019-03-29 21:19:19','2019-03-30 02:19:19',0,NULL,NULL),
(4,'AAPL','Apple Inc.','2019-03-29 21:31:03','2019-03-30 02:31:03',0,NULL,NULL),
(5,'IQ','iQIYI Inc.','2019-03-29 22:45:30','2019-03-30 03:45:30',0,NULL,NULL),
(6,'GOOGL','Alphabet Inc.','2019-03-30 00:44:53','2019-03-30 05:44:53',0,NULL,NULL),
(7,'AMZN','Amazon.com Inc.','2019-03-30 00:45:10','2019-03-30 05:45:10',0,NULL,NULL),
(8,'NVDA','NVIDIA Corporation','2019-03-31 20:48:33','2019-04-01 01:48:33',0,NULL,NULL),
(9,'TWLO','Twilio Inc. Class A','2019-04-01 13:28:42','2019-04-01 18:28:42',0,NULL,NULL),
(12,'FB','Facebook Inc.','2019-04-04 23:16:52','2019-04-05 04:16:52',0,NULL,NULL),
(13,'DIS','The Walt Disney Company','2019-04-04 23:17:09','2019-04-05 04:17:09',0,NULL,NULL),
(14,'SPY','SPDR S&P 500','2019-04-05 10:12:52','2019-04-05 15:12:52',0,NULL,NULL),
(15,'BDD',NULL,'2019-04-05 22:36:48','2019-04-06 03:36:48',0,NULL,NULL),
(16,'AA','Alcoa Corporation','2019-04-05 22:57:00','2019-04-06 03:57:00',0,NULL,NULL),
(17,'AAP',NULL,'2019-04-07 23:35:56','2019-04-08 04:35:56',0,NULL,NULL),
(18,'ICHR',NULL,'2019-04-10 17:05:59','2019-04-10 22:05:59',0,NULL,NULL),
(19,'CMG',NULL,'2019-04-10 18:10:07','2019-04-10 23:10:07',0,NULL,NULL),
(20,'NIO',NULL,'2019-04-10 18:13:35','2019-04-10 23:13:35',0,NULL,NULL),
(21,'AMAT',NULL,'2019-04-10 18:27:46','2019-04-10 23:27:46',0,NULL,NULL),
(22,'XLNX',NULL,'2019-04-10 18:28:30','2019-04-10 23:28:30',0,NULL,NULL),
(23,'WDAY','Workday Inc.','2019-04-10 20:45:59','2019-04-11 01:45:59',0,NULL,NULL),
(24,'UNH',NULL,'2019-04-12 22:36:24','2019-04-13 03:36:24',0,NULL,NULL),
(25,'CAAP',NULL,'2019-04-12 22:39:30','2019-04-13 03:39:30',0,NULL,NULL),
(26,'BIGC','BigCommerce Holdings, Inc.',NULL,'2020-11-27 22:19:50',0,'2020-11-28 04:19:50','2020-11-28 04:19:50'),
(27,'GM','General Motors Co.',NULL,'2020-11-27 22:20:33',0,'2020-11-28 04:20:33','2020-11-28 04:20:33'),
(28,'F','Ford Motor Co.',NULL,'2020-11-27 23:23:39',0,'2020-11-28 05:23:39','2020-11-28 05:23:39'),
(29,'AA','Alcoa Corp.',NULL,'2020-11-28 23:00:53',0,'2020-11-29 05:00:53','2020-11-29 05:00:53'),
(30,'AACQU','Artius Acquisition Inc. Units Cons of 1 Shs A + 1/3 Wt 13.07.25',NULL,'2020-11-28 23:04:12',0,'2020-11-29 05:04:12','2020-11-29 05:04:12');

/*!40000 ALTER TABLE `stocks` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_friends
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_friends`;

CREATE TABLE `user_friends` (
                                `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                                `user_id` int(11) unsigned NOT NULL,
                                `friend_user_id` int(11) unsigned NOT NULL,
                                `unfriended_by_user_id` int(11) unsigned DEFAULT NULL,
                                `status_id` int(11) unsigned NOT NULL,
                                `created` timestamp NULL DEFAULT NULL,
                                `modified` datetime DEFAULT CURRENT_TIMESTAMP,
                                `deleted` tinyint(1) DEFAULT '0',
                                PRIMARY KEY (`id`),
                                KEY `user_friends_to_user_fk_1` (`user_id`),
                                KEY `user_friends_to_friend_user_fk_2` (`friend_user_id`),
                                KEY `user_friends_to_unfriended_by_user_fk_3` (`unfriended_by_user_id`),
                                KEY `user_friends_status_fk_4` (`status_id`),
                                CONSTRAINT `user_friends_status_fk_4` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                                CONSTRAINT `user_friends_to_friend_user_fk_2` FOREIGN KEY (`friend_user_id`) REFERENCES `users_bu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                                CONSTRAINT `user_friends_to_unfriended_by_user_fk_3` FOREIGN KEY (`unfriended_by_user_id`) REFERENCES `users_bu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                                CONSTRAINT `user_friends_to_user_fk_1` FOREIGN KEY (`user_id`) REFERENCES `users_bu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table user_ratings_summary
# ------------------------------------------------------------

# Error: SELECT command denied to user ''@'%' for column 'id' in table 'users'


# Dump of table user_reputation_summary
# ------------------------------------------------------------

# Error: SELECT command denied to user ''@'%' for column 'id' in table 'users'


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
                         `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                         `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                         `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                         `email_verified_at` timestamp NULL DEFAULT NULL,
                         `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                         `two_factor_secret` text COLLATE utf8mb4_unicode_ci,
                         `two_factor_recovery_codes` text COLLATE utf8mb4_unicode_ci,
                         `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `created_at` timestamp NULL DEFAULT NULL,
                         `updated_at` timestamp NULL DEFAULT NULL,
                         PRIMARY KEY (`id`),
                         UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `two_factor_secret`, `two_factor_recovery_codes`, `remember_token`, `created_at`, `updated_at`)
VALUES
(1,'brandon worby','brandon@bworby.com',NULL,'$2y$10$0vTDCVakWl9QJX0qKP4aTO73ov2GKs6uB/0.uT9BVZTNgGAFwyJKu',NULL,NULL,NULL,'2020-11-22 22:06:52','2020-11-22 22:06:52'),
(2,'asd','ddd@asdfasd.com',NULL,'$2y$10$0vTDCVakWl9QJX0qKP4aTO73ov2GKs6uB/0.uT9BVZTNgGAFwyJKu',NULL,NULL,NULL,'2020-11-22 22:06:52','2020-11-22 22:06:52'),
(3,'dddd','ddd@asdfddasd.com',NULL,'$2y$10$0vTDCVakWl9QJX0qKP4aTO73ov2GKs6uB/0.uT9BVZTNgGAFwyJKu',NULL,NULL,NULL,'2020-11-22 22:06:52','2020-11-22 22:06:52'),
(4,'dd2222','d333@asdfdddasd.com',NULL,'$2y$10$0vTDCVakWl9QJX0qKP4aTO73ov2GKs6uB/0.uT9BVZTNgGAFwyJKu',NULL,NULL,NULL,'2020-11-22 22:06:52','2020-11-22 22:06:52'),
(5,'d333333','d3dd33@dss.com',NULL,'$2y$10$0vTDCVakWl9QJX0qKP4aTO73ov2GKs6uB/0.uT9BVZTNgGAFwyJKu',NULL,NULL,NULL,'2020-11-22 22:06:52','2020-11-22 22:06:52'),
(6,'dd3f3tg','dd@dss5.com',NULL,'$2y$10$0vTDCVakWl9QJX0qKP4aTO73ov2GKs6uB/0.uT9BVZTNgGAFwyJKu',NULL,NULL,NULL,'2020-11-22 22:06:52','2020-11-22 22:06:52');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table votes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `votes`;

CREATE TABLE `votes` (
                         `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                         `value` tinyint(1) DEFAULT '0',
                         `user_id` int(11) unsigned DEFAULT NULL,
                         `stock_id` int(11) unsigned DEFAULT NULL,
                         `comment_id` int(11) unsigned NOT NULL,
                         `comment_owner_user_id` int(11) unsigned DEFAULT NULL,
                         `created` timestamp NULL DEFAULT NULL,
                         `modified` datetime DEFAULT CURRENT_TIMESTAMP,
                         `deleted` tinyint(1) DEFAULT '0',
                         PRIMARY KEY (`id`),
                         KEY `votes_user_fk_1` (`user_id`),
                         KEY `votes_stock_fk_2` (`stock_id`),
                         KEY `votes_comment_fk_3` (`comment_id`),
                         KEY `votes_comment_owner_user_id_fk_4` (`comment_owner_user_id`),
                         CONSTRAINT `votes_comment_fk_3` FOREIGN KEY (`comment_id`) REFERENCES `comments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                         CONSTRAINT `votes_comment_owner_user_id_fk_4` FOREIGN KEY (`comment_owner_user_id`) REFERENCES `users_bu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                         CONSTRAINT `votes_stock_fk_2` FOREIGN KEY (`stock_id`) REFERENCES `stocks` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                         CONSTRAINT `votes_user_fk_1` FOREIGN KEY (`user_id`) REFERENCES `users_bu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `votes` WRITE;
/*!40000 ALTER TABLE `votes` DISABLE KEYS */;

INSERT INTO `votes` (`id`, `value`, `user_id`, `stock_id`, `comment_id`, `comment_owner_user_id`, `created`, `modified`, `deleted`)
VALUES
(57,1,4,NULL,119,4,'2019-04-06 00:00:54','2019-04-06 05:00:54',0),
(58,1,4,NULL,119,4,'2019-04-06 00:00:54','2019-04-06 05:00:54',0),
(59,0,4,NULL,119,4,'2019-04-06 00:00:54','2019-04-06 05:00:54',0),
(60,1,4,NULL,119,3,'2019-04-06 00:00:54','2019-04-06 05:00:54',0),
(61,1,4,NULL,131,NULL,'2019-04-15 22:17:40','2019-04-16 03:17:42',0),
(62,1,3,NULL,132,NULL,'2020-04-04 22:49:29','2020-04-05 03:49:29',0);

/*!40000 ALTER TABLE `votes` ENABLE KEYS */;
UNLOCK TABLES;


/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
