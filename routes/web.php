<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StockController;
use App\Http\Controllers\PortfolioController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::post('stock/addComment', [StockController::class,'addComment'])->name('stockAddComment')->middleware('auth');
Route::get('/stocks', [StockController::class,'stocks']);
Route::get('/stocks/{symbol}', [StockController::class,'stock']);
Route::patch('comments/{comment}',[\App\Http\Controllers\CommentsController::class,"update"])->name('editComment')->middleware('auth');
Route::post('comments/{comment}',[\App\Http\Controllers\CommentsController::class,"update"])->name('editComment')->middleware('auth');

Route::get('/portfolios', [PortfolioController::class,'portfolios'])->middleware('auth');
Route::get('/portfolio/{id}', [PortfolioController::class,'portfolio'])->middleware('auth');
Route::post('portfolio/create', [PortfolioController::class,'createPortfolio'])->name('createPortfolio')->middleware('auth');
Route::post('portfolio/addStock', [PortfolioController::class,'addStockToPortfolio'])->name('addStockToPortfolio')->middleware('auth');


Route::get('/stocksdata/quoteAsGet', [StockController::class, 'quoteAsGet'])->name('stocksdata.quoteAsGet');
Route::get('/stocksdata/getChartData', [StockController::class, 'getChartData'])->name('stocksdata.getChartData');
Route::post('/stocksdata/quoteAsPost', [StockController::class, 'quoteAsPost'])->name('stocksdata.quoteAsPost');
Route::get('/stocksdata/search', [StockController::class, 'search'])->name('stocksdata.search');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

